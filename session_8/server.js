var http = require('http');

var dummyTasks = [
  {
      id: 1,
      name: "tarefa teste",
      isDone: false
  },
  {
      id: 2,
      name: "tarefa teste 2",
      isDone: false
  },
  {
      id: 3,
      name: "tarefa teste 3",
      isDone: true
  },
  {
      id: 4,
      name: "tarefa teste 4",
      isDone: false
  },
  {
    id: 5,
    name: "tarefa teste 5",
    isDone: true
}
];

http.createServer(function (req, res) {
  res.writeHead(200, {'Content-Type': 'application/json', 'Access-Control-Allow-Origin': 'http://127.0.0.1' });
  res.write(JSON.stringify(dummyTasks, null, 3));
  res.end();
}).listen(8080);
