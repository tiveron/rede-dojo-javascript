angular.module('taskListApp', [])

    .controller('TodoListController', function($scope, Tasks) {

        $scope.tasks = [];
        $scope.markAsDone = function(task) {
            task.isDone = true;
        }
        $scope.delete = function(id) {

            for(task in $scope.tasks) {
                if($scope.tasks[task].id === id) {
                    $scope.tasks.splice(task, 1);
                }
            }
        }

        function init() {
            getTasks();
        }

        function getTasks() {
            Tasks.getTasks().then(function(response){
                $scope.tasks = response.data;
            }).catch(function(error){
                console.log('SEM TASKS');
            });
        }

        init();
    })

    .service('Tasks', function($http) {
        this.getTasks = function() {
            return $http({
				url: 'http://localhost:8080/tasks',
				method: 'GET',
				data: {}
			});
        }

    })
