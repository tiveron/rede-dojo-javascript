QUnit.test( "hello test", function( assert ) {

    assert.equal(multiplica(2,2), 4, "multiplicacao de 2 parametros");
    
    assert.equal(multiplica(2,3), 6, "multiplicacao de 2 parametros");

    assert.equal(multiplica(2),2,  "multiplicacao de 2 parametros");

    assert.equal(multiplica(0),0,  "multiplicacao de 2 parametros");

    assert.equal(multiplica(2,3,2), 12, "multiplicacao de 3 parametros");

    assert.equal(multiplica(), 0, "não passar parametros");

    assert.equal(multiplica("Lixo"), 0, "não numerico");
});
  