
function multiplica(){

    var argumentos = Array.from(arguments);
    var result = 1;
    var fator;

    if (argumentos.length == 0){
        return 0;
    }

    for (var contador = 0; contador < argumentos.length; contador++) {
        fator = argumentos[contador];
        
        if(isNaN(fator)){
            return 0;
        } 

        result *= fator;   
    }
    
    return result;
}