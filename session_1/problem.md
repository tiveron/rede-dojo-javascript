# O problema

**Multiplicação de números**

Desenvolver uma função que multiplique os números passados por parâmetro.

 - Caso não passe nenhum parâmetro, deve retornar 0.
 - Caso passe somente um parâmetro, deve retornar o número passado.
 - Somente números deverão ser aceitos, caso contrário deve retornar 0.