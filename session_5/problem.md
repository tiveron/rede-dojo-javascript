# O problema

**Componente - filtro**

Desenvolver uma função receba uma string e uma lista de strings:

 - A string será um filtro para a lista
 - Os itens da lista que não contém o termo do filtro deve ser removidos
 - A lista filtrada deve ser retornada

Bônus

 - Implementar a função em uma página.
 - A função criada deve ser encapsulada, utilizando objeto literal
 - O nome do objeto deve ser "componentes"
 - O objeto "componentes" deve ter 3 métodos:
  - init: será o inicializador do nosso componente, responsável por buscar e iniciar todos os componentes da página
  - bindEvents: será utilizado para registrar os eventos do componente
  - a função criada no dojo
 - Os testes unitários deverão ser refatorados para essa nova estrutura
 - Todos os componentes criados nas sessões seguintes farão parte do mesmo objeto
 - A página deverá ter um container para encapsular o componente
 - O componente deverá ter uma classe (componente = reutilização)
 - O container deverá ter um input que servirá de filtro
 - O container deverá ter um outro container para encapsular a lista de strings
 - A lista de strings deverá ser uma tag de lista desordenada (tag <ul>)
 - Deverá ser criada uma classe CSS de nome "hide" para esconder os itens da lista que não estão no resultado do filtro
 - A função será disparada no evento "keyup" do input
 - O conceito de componente precisa ser testado, ou seja, podemos ter 2 componentes na mesma página e os dois precisam funcionar de maneira independente
