QUnit.test("filtro", function( assert ) {
    assert.deepEqual(componentes.filtro.filtrar("teste",["teste1","teste2"]),["teste1","teste2"],"deve retornar teste1,teste2");
    assert.deepEqual(componentes.filtro.filtrar("teste",["teste1","teste2", "teste3"]),["teste1","teste2", "teste3"],"deve retornar teste1,teste2,teste3");
    assert.deepEqual(componentes.filtro.filtrar("teste",["teste1","teste2", "teste3","sessão"]),["teste1","teste2", "teste3"],"deve retornar teste1,teste2,teste3");
    assert.deepEqual(componentes.filtro.filtrar("",["teste1","teste2", "teste3","sessão"]),["teste1","teste2", "teste3","sessão"],"deve retornar teste1,teste2,teste3,sessão"); 
    assert.deepEqual(componentes.filtro.filtrar(" ",["teste1","teste2", "teste3","sessão"]),[],"deve retornar nada");
    assert.deepEqual(componentes.filtro.filtrar(" ",["teste1","teste2", "teste 3","sessão"]),["teste 3"],"deve retornar teste 3");
});
