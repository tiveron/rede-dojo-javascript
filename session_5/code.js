var componentes = {
    filtro: {
        // função responsável por iniciar o componente de filtro. Ele busca por todas as classes do componente e
        // atribui os eventos pra cada um deles, para que possam funcionar de forma independente.
        init: function(){
            var elementoComponentes = document.querySelectorAll(".containerFiltro");
            for(var index = 0, total = elementoComponentes.length; index < total; index++) {
                componentes.filtro.bindEvents(elementoComponentes[index])
            }
        },
        // função responsável registrar os eventos em nosso componente.
        bindEvents: function(componente) {
            var input = componente.querySelector(".filtro"),
                elementoPalavras = componente.querySelectorAll("li")
                palavras = [];
            var totalPalavras = elementoPalavras.length;

                for(var index = 0; index < totalPalavras; index++) {
                    palavras.push(elementoPalavras[index].textContent);
                }

                input.addEventListener('keyup', function() {
                    var listaFiltrada = componentes.filtro.filtrar(input.value, palavras);
                    for(var index = 0; index < totalPalavras; index++) {
                        if(listaFiltrada.indexOf(elementoPalavras[index].textContent) == -1) {
                            if(!elementoPalavras[index].classList.contains('hide')) {
                                elementoPalavras[index].classList.add('hide');
                            }
                        } else {
                            if(elementoPalavras[index].classList.contains('hide')) {
                                elementoPalavras[index].classList.remove('hide');
                            }
                        }
                    }
                });
        },
        // função que fizemos no dojo. A "regra de negócio".
        filtrar: function(palavra, lista){
            var listaFiltro = [],
                regexp = new RegExp(palavra, 'gi');

            for (var i = 0, total = lista.length; i < total; i++) {
                if(lista[i].match(regexp)){
                    listaFiltro.push(lista[i]);
                }
            }
            return listaFiltro;
        }
    }
}
