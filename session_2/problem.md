# O problema

**Fizz Buzz**

Desenvolver uma função receba um número e retorne de acordo com os seguintes critérios:.

 - Deve retornar a string "fizz", caso número seja múltiplo de 3.
 - Deve retornar a string "buzz", caso número seja múltiplo de 5.
 - Deve retornar a string "fizzbuzz", caso número seja múltiplo de 3 e 5.
 - Caso contrário, deve retornar o próprio número recebido.
 - Deve aceitar somente números (em caso de argumento inválido, ou ausência de argumento, deve retornar falso)

Bônus

 - Criar uma função que receba 2 números (inicio e fim).
 - A função deve receber 2 argumentos para ser válida. 
 - Deve retornar todos os números desse intervalo no padrão fizzbuzz.
 - O número inicial deve ser menor que o final.
 - Somente números deverão ser aceitos.
 - exemplo: argumentos (0,4) > retorno: "fizzbuzz12fizz4"
