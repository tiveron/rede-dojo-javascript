QUnit.test( "teste para função testeFizzBuzz", function( assert ) {
    assert.equal(testeFizzBuzz(4),"4","deve retornar 4");
    assert.equal(testeFizzBuzz(6),"fizz","deve retornar fizz");
    assert.equal(testeFizzBuzz(10),"buzz","deve retornar buzz");
    assert.equal(testeFizzBuzz(15),"fizzbuzz","deve retornar fizzbuzz");
    assert.notOk(testeFizzBuzz('A'),"deve retornar false");
    assert.equal(testeFizzBuzz(),false,"deve retornar false");
    assert.equal(testeFizzBuzz(0),"fizzbuzz","deve retornar fizzbuzz");
    assert.equal(testeFizzBuzz(-10),"buzz","deve retornar buzz");
    assert.equal(testeFizzBuzz(-10.5),-10.5,"deve retornar -10.5");
});

QUnit.test( "teste para função retornaIntervalo", function( assert ) {
    assert.equal(retornaIntervalo(2,4),"2fizz4","deve retornar 2fizz4");
    assert.equal(retornaIntervalo(1,4),"12fizz4","deve retornar 12fizz4");
    assert.equal(retornaIntervalo(0,5),"fizzbuzz12fizz4buzz","deve retornar fizzbuzz12fizz4buzz");
    assert.strictEqual(retornaIntervalo(5,0),false,"deve retornar false");
    assert.strictEqual(retornaIntervalo('A'),false,"so aceita  numeros");
    assert.strictEqual(retornaIntervalo('A',5),false,"so aceita  numeros");
    assert.strictEqual(retornaIntervalo(5,'A'),false,"so aceita  numeros");
    assert.strictEqual(retornaIntervalo(1),false,"numero de parametros irregulares");
    assert.strictEqual(retornaIntervalo(1,2,3),false,"numero de parametros irregulares");
    assert.strictEqual(retornaIntervalo(-1,2),"-1fizzbuzz12","deve retornar -1fizzbuzz12");
    assert.strictEqual(retornaIntervalo(5,2),false,"deve retornar falso");
});