function testeFizzBuzz(x) {
    if(isNaN(x)){
        return false;
    }

    if(x % 3 === 0 && x % 5 === 0){
        return "fizzbuzz";
    } 

    if(x % 3 === 0){
        return "fizz";
    }

    if(x % 5 === 0){  
        return "buzz";      
    }

    return x;
}

function retornaIntervalo(inicio, fim){

    if((arguments.length !== 2) || (isNaN(inicio) || isNaN(fim)) || (inicio>=fim)) {
        return false;
    }

    var resultado = '';

    for(var i = inicio ; i <= fim ;  i++  ) {
        resultado += testeFizzBuzz(i);
    }

    return resultado;
}