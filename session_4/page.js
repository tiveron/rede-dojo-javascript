window.addEventListener("load", function(event) {

    var input = document.querySelector("#pwdSenha");
    var scoreElement = document.querySelector("#score");

    input.addEventListener("keyup", function(event){
        var score = forcaSenha(input.value);
        console.log(score); 
            scoreElement.classList = "";
        if (score > 0 && score <= 25) {
            
            scoreElement.classList.add("senhaFraca");
        }    
        if (score > 25 && score <= 50) {
            
            scoreElement.classList.add("senhaModerada");
        }    
        if (score > 50 && score <= 75) {
            
            scoreElement.classList.add("senhaForte");
        }    
        if (score > 75 && score <= 100) {
            
            scoreElement.classList.add("senhaMuitoForte");
        } 
    })

});