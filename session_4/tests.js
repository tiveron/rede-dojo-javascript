QUnit.test( "Validar Minuscula", function( assert ) {
    assert.equal(validaMinusculas("testes"), 15 , "retorna score = 15");
});

QUnit.test( "Validar Maiúscula", function( assert ) {
    assert.equal(validaMaiusculas("testeS"), 15 , "retorna score = 15");
});

QUnit.test( "Validar Tamanho", function( assert ) {
    assert.equal(validaTamanho("testeSs"), 10 , "retorna score = 10");
});

QUnit.test( "Validar Numero", function( assert ) {
    assert.equal(validaNumeros("testeSs1"), 30 , "retorna score = 30");
});

QUnit.test( "Validar Numero", function( assert ) {
    assert.equal(validaEspeciais("testeSs1@"), 30 , "retorna score = 30");
});

QUnit.test( "descrição do teste", function( assert ) {
    assert.equal(forcaSenha("testes"), 15 , "retorna score = 15");
    assert.equal(forcaSenha("testeS"), 30, "retorna score = 30");
    assert.equal(forcaSenha("testeSs"), 40 , "retorna score = 40");
    assert.equal(forcaSenha("testeSs1"), 70 , "retorna score = 70");
    assert.equal(forcaSenha("testeSs1@"), 100 , "retorna score = 100");
    assert.equal(forcaSenha(""), 0 , "retorna score = 0");
});
