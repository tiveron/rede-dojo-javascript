function validaMinusculas(senha) {
   if(senha.match(/[a-z]/)){
       return 15;
   }

    return 0;
}

function validaMaiusculas(senha) {
    if(senha.match(/[A-Z]/)){
        return 15;
    }
    return 0;
}

function validaTamanho(senha){
    if(senha.length > 6){
        return 10;
    }
    return 0;
}

function validaNumeros(senha) {
    if(senha.match(/[0-9]/)){
        return 30;
    }
 
     return 0;
}

function validaEspeciais(senha) {
    
    if(senha.match(/[\!\@\#\$\%\¨\&\*\_\+\=\(\)\[\]\{\}]/)){
        return 30;
    }   
    return 0;

}

function forcaSenha(senha) {
    return validaMinusculas(senha) 
    + validaMaiusculas(senha) 
    + validaTamanho(senha)
    + validaNumeros(senha)
    +validaEspeciais(senha);
}

