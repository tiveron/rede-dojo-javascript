# O problema

**Força de senha**

Desenvolver uma função receba uma string contendo uma senha, e a classifique de acordo com as seguintes regras:


 - contenha letras minusculas: +15 pontos
 - contenha letras maiusculas: +15 pontos
 - contenha mais de 6 caracteres: +10 pontos
 - contenha números: +30 pontos
 - contenha caracteres especiais: +30 pontos

Bônus

 - Implementar a função em uma página.
 - A página deverá ter 1 input do tipo password para receber a senha
 - A página deverá ter uma div que será usada como uma barra de indicador de força de senha
 - A força de senha deve ser acionada quando o usuário começar a digitar no campo de senha
 - A barra deverá ter 4 estados:
  - senha fraca: 1 a 25 pontos; a barra deverá ser da cor vermelha e de tamanho 25%;
  - senha moderada: 26 a 50 pontos; a barra deverá ser da cor amarela e de tamanho 50%;
  - senha forte: 51 a 75 pontos; a barra deverá ser da cor amarela e de tamanho 75%;
  - senha muito forte: 75 a 100 pontos; a barra deverá ser da cor verde e de tamanho 100%;