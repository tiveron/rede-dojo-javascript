# Dojo Javascript/AngularJS 

Repositório criado para centralizar as sessões de coding dojo utilizando javascript/angularJS na Rede.

Após cada sessão, criaremos uma pasta com o número da sessão. Dentro desta pasta você vai encontrar a descrição do problema, assim como a solução implementada durante a sessão.