window.addEventListener("load", function(event) {

    var button = document.querySelector("#btn");
    var texto1 = document.querySelector("#frase");
    var texto2 = document.querySelector("#fraseInversa");

    button.addEventListener("click", function(event){
        if(texto1.value.length){

             texto2.value = recebeFrase(texto1.value);
             texto2.classList.remove("invalido");
             texto2.classList.add("valido");
            
        }else{
            texto2.classList.remove("valido");
            texto2.classList.add("invalido");
        }
    });
    
});