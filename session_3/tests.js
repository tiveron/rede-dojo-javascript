QUnit.test( "Tester Inverter palavra", function( assert ) {

assert.equal(inverterPalavra("TERRAS"),"SARRET","deve retornar SARRET");
assert.equal(inverterPalavra("TERRA"),"TERRA","deve retornar TERRA");

});

QUnit.test( "Recebe Frase", function( assert ) {

    assert.equal(recebeFrase("TERRAS SARRET"),"SARRET TERRAS","deve retornar TERRA");
    assert.equal(recebeFrase(123),false,"deve retornar Frase inválida!");
    assert.equal(recebeFrase("SUBI NA ARVORE"),"SUBI NA EROVRA","deve retornar SUBI NA EROVRA");
});
  