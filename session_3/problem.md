# O problema

**Spin Words**

Desenvolver uma função receba uma frase e retorne de acordo com os seguintes critérios:

 - Palavras com mais de 5 letras, devem ser invertidas.
 - Palavras com 5 ou menos letras não devem ser alteradas.
 - Em caso de argumento inválido, deve retornar a string "Frase inválida!".
 - Em caso de falta de argumento, deve retornar a string "Frase não encontrada!".

Bônus

 - Implementar a função em uma página.
 - Para ficar mais coerente, alterar a função para retornar false em caso de erro (ao invés das mensagens)
 - A página deverá ter 2 inputs.
    - um input para o usuário escrever a frase.
    - um input readonly para ser exibido o resultado.
 - A página deverá ter um botão para aplicar a função ao ser clicado.
 - Quando o resultado for inválido, a borda do input que exibe o resultado deve ficar vermelha.
 - Quando o resultado for válido, a borda do input que exibe o resultado deve ficar verde.
 - Aplicar as cores através de classes CSS para erro e sucesso