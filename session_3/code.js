function inverterPalavra(str) {
    
    if(str.length <= 5)
    {
        return str;
    }

    return str.split("").reverse().join("");

}

function recebeFrase(frase){

    if(typeof frase != "string"){
        
        return false;

    }
    
    var palavras = frase.split(" ");
    var fraseinversa = [];

    for(var key in palavras){
        fraseinversa.push(inverterPalavra(palavras[key]));
    }

      return fraseinversa.join(" ");

}