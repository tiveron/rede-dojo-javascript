var componentes = {
    tasks: { 
        //Função inicializadora
        init:function(config) {
             var container = componentes.tasks.createDOM(config.target, config.title,config.id);
             componentes.tasks.bindEvents(container);
        },
        // Cria os elementos do Componente
        createDOM:function(target, titleText,id){
            var container= document.createElement('div');
            container.classList.add("taskList");

            container.id = id;

            var title = document.createElement('h3');
            title.textContent = titleText;

            var input = document.createElement('input');
            input.type="text";
            input.placeholder="Digite sua tarefa";

            var button = document.createElement('button');
            button.textContent="Salvar";

            var lista = document.createElement('ul');
            lista.classList.add('list');

            container.appendChild(title);
            container.appendChild(input);
            container.appendChild(button);
            container.appendChild(lista);

            target.appendChild(container);
            return container;
        },
        //Adiciona eventos ao nosso componente
        bindEvents:function(target){
            var button = target.querySelector("button");
            var input = target.querySelector("input");
            var tasksList = target.querySelector('.list');

            button.addEventListener("click",function(){
                componentes.tasks.createTask(tasksList, input.value);
                input.value = "";
            });

        },
        // Cria a lista de tarefas
        createTask:function(target, name){
            if (name){
                var task = document.createElement('li');
                task.textContent = name;

                var link = document.createElement('a');
                link.textContent = 'X';
                link.href = '#';

                task.appendChild(link);
                target.appendChild(task);
                componentes.tasks.bindTaskEvents(task);
            }

        },
        // Adiciona eventos à tarefa criada
        bindTaskEvents: function(target) {
            var link =  target.querySelector("a");

            target.addEventListener("click", function(){
                target.classList.toggle('done');
            });

            link.addEventListener("click", function(){
                componentes.tasks.deleteTask(target);
            });
        },

        deleteTask: function(target){
            target.parentNode.removeChild(target);
        }
    }
};
