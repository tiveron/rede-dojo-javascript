# O problema

**Componente - to do list**

Desenvolver um componente de lista de tarefas, seguindo os seguintes critérios:

HTML:
 - O componente deverá ter um container
 - Deverá ter um título, utilizando a tag h3
 - Deverá ter um input para entrada de dados
 - Deverá ter um botão para gravar os dados
 - Deverá ter uma lista para as tarefas cadastradas
 - Os itens da lista deverão ter um link com texto "x" para deletar a tarefa

CSS:
 - Os itens da lista deverão ser zebrados (branco e cinza claro)
 - Os itens da lista deverão mudar de cor ao passar o ponteiro do mouse (cinza escuro)
 - Deverá ser criada uma classe para tratar tarefas feitas

Javascript:
 - Na inicialização do componente, será passado um objeto de configuração contendo:
  - 1) elemento alvo, onde o componente será inserido (caso não passe, deverá ser inserido na tag body)
  - 2) título do componente. Será colocado na tag h3
 - Ao adicionar um item, o input deve ser zerado
 - Ao clicar em uma tarefa já cadastrada, a tarefa deve ser marcada como feita
 - Ao tentar adicionar uma tarefa sem texto, uma mensagem de erro deve ser exibida
 - Ao clicar no "x" da tarefa, a mesma deve ser excluída
