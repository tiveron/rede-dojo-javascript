window.addEventListener("load", function(event) {
    var container = document.querySelector("#container");

    componentes.tasks.init({
        target: container,
        title: "To do",
        id:"componente1"
    });
    componentes.tasks.init({
        target: container,
        title: "Doing",
        id:"componente2"
    });
    componentes.tasks.init({
        target: container,
        title: "Done",
        id:"componente3"
    });
});
